import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';

// ngrx related imports
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  assetsReducer,
  initialState as AssetsState,
} from './store/home/home.reducer';
import { HomeEffects } from './store/home/home.effects';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    // ngrx related imports
    // StoreModule.forRoot(reducers, {
    //   metaReducers,
    // }),
    // EffectsModule.forRoot([AssestsEffects]),
    StoreModule.forRoot(
      {
        assets: assetsReducer,
      },
      {
        initialState: {
          assets: AssetsState,
        },
        //metaReducers: !environment.production ? [storeFreeze] : []
      }
    ),
    EffectsModule.forRoot([HomeEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
