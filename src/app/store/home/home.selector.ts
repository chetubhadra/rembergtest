import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AssetsState } from './home.reducer';

const getAssetsState = createFeatureSelector<AssetsState>('assets');

const getLoaded = createSelector(
  getAssetsState,
  (state: AssetsState) => state.loaded
);
const getError = createSelector(
  getAssetsState,
  (state: AssetsState) => state.error
);
const getAllAssets = createSelector(
  getAssetsState,
  getLoaded,
  (state: AssetsState, isLoaded) => {
    return isLoaded ? state.list : [];
  }
);

const getAllSelectedAssets = createSelector(
  getAssetsState,
  getLoaded,
  (state: AssetsState, isLoaded) => {
    return isLoaded ? state.selectedList : undefined;
  }
);

const getAddSelectedAssetsResponse = createSelector(
  getAssetsState,
  getLoaded,
  (state: AssetsState, isLoaded) => {
    return isLoaded ? state.addSelectedResponse : undefined;
  }
);

export const assetsQuery = {
  getLoaded,
  getError,
  getAllAssets,
  getAllSelectedAssets,
  getAddSelectedAssetsResponse
};
