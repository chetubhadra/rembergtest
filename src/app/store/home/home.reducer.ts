import { AssetsAction, AssetsActionTypes } from './home.actions';

export interface AssetsState {
  list: any[];
  selectedList: any;
  addSelectedResponse: any;
  selectedId?: string | number;
  loaded: boolean;
  error?: any;
}

export const initialState: AssetsState = {
  list: [],
  selectedList: null,
  addSelectedResponse: null,
  loaded: false,
};

export function assetsReducer(
  state: AssetsState = initialState,
  action: AssetsAction
): AssetsState {
  switch (action.type) {
    case AssetsActionTypes.LoadAssetsSuccess: {
      state = {
        ...state,
        list: action.payload,
        loaded: true,
      };
      break;
    }
    case AssetsActionTypes.SelectedLoadAssetsSuccess: {
      state = {
        ...state,
        selectedList: action.payload,
        loaded: true,
      };
      break;
    }
    case AssetsActionTypes.AddSelectedAssetsSuccess: {
      state = {
        ...state,
        addSelectedResponse: action.payload,
        loaded: true,
      };
      break;
    }
  }
  return state;
}
